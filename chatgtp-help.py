# ChatGPT Extension for Thonny IDE
# Written by ChatGPT
# Debugged by celleron56
#under unlicense 
#to install place in thonny extension folder (under tools is chatgtp option)
#make sure you have required libraries installed
import thonny
from thonny import get_workbench
import requests
from tkinter import Tk, simpledialog, messagebox, Text, Scrollbar,font
from tkinter import scrolledtext
from pygments import highlight
import pygments
from pygments.lexers import PythonLexer
from chlorophyll import CodeView


API_ENDPOINT = "https://free.churchless.tech/v1/chat/completions"
API_KEY = ""
MAX_CHAT_HISTORY = 10

chat_history = []

def send_prompt_to_chatgpt(prompt):
    data = {
        "messages": chat_history + [{"role": "user", "content": prompt}]
    }

    headers = {
        "Authorization": f"Bearer {API_KEY}"
    }

    response = requests.post(API_ENDPOINT, json=data, headers=headers)

    if response.status_code == 200:
        try:
            response_data = response.json()
            choices = response_data.get("choices", [])
            if choices:
                reply = choices[0].get("message", {}).get("content", "Error: Invalid response format from ChatGPT API")
                chat_history.append({"role": "assistant", "content": reply})
                if len(chat_history) > MAX_CHAT_HISTORY:
                    chat_history.pop(0)
                return reply
            else:
                return "Error: Empty response from ChatGPT API"
        except json.JSONDecodeError:
            return "Error: Invalid JSON response from ChatGPT API"
    else:
        return "Error: Unable to get response from ChatGPT API"

def format_code(code):
    root = Tk()
    root.title("Formatted Code")

    code_box = CodeView(root, lexer=pygments.lexers.PythonLexer, wrap="word", color_scheme="monokai")
    code_box.pack(fill="both", expand=True)

    formatted_code = code
    code_box.insert("1.0", formatted_code)
    code_box.configure(state="disabled")

    root.mainloop()

def chat_with_gpt_command():
    root = Tk()
    root.withdraw()

    prompt = simpledialog.askstring("Chat with GPT", "Enter your prompt:")

    response = send_prompt_to_chatgpt(prompt)

    code_blocks = response.split("```")
    code_blocks = code_blocks[1:] if len(code_blocks) > 1 else code_blocks

    if code_blocks:
        for code_index, code in enumerate(code_blocks):
            code = code.strip()
            if code:
                format_code(code)
    else:
        messagebox.showinfo("ChatGPT Response", response)

def load_plugin():
    workbench = thonny.get_workbench()

    menu_name = "tools"
    command_label = "Chat with GPT"
    workbench.add_command(
        command_id="chat_with_gpt_command",
        menu_name=menu_name,
        command_label=command_label,
        handler=chat_with_gpt_command
    )